const crypto = require('crypto')
const nodemailer = require('nodemailer')
const redis = require('redis')
require('dotenv').config()

class RedisProvider {
  constructor() {
    this.client = null
    this.status = null

    return this
  }

  disconnect() {
    this.client.quit()
  }

  connect() {
    if (!this.client) this.client = redis.createClient()

    // errors? sure, lets see em
    this.client.on('error', err => {
      this.status = 'error'
      console.log(err)
    })

    this.client.on('ready', () => {
      this.status = 'ready'
      console.log('REDIS: ready')
    })

    return this.client
  }

  set(key, value) {
    if (!this.client) return Promise.resolve(null)
    return new Promise((resolve, reject) => {
      this.client.set(key, value, (err, reply) => {
        // callback
        console.log('SET redis:', reply)
        resolve(reply)
      })
    })
  }

  get(key) {
    if (!this.client) return Promise.resolve(null)
    return new Promise((resolve, reject) => {
      this.client.get(key, (err, reply) => {
        if (err) {
          reject(err)
          return
        }

        // reply is null when the key is missing
        console.log('GET redis:', reply)
        resolve(reply)
      })
    })
  }
}

const r = new RedisProvider()
r.connect()

setTimeout(async () => {
  // NOTE: Generates a TESTING One Time Hash
  const oneTimeHash = contractAddress => {
    const uuid = crypto.randomBytes(16).toString("hex")
    console.log('uuid, contractAddress', uuid, contractAddress)
    r.set(uuid, contractAddress)
    return uuid
  }

  const accountAddress = '0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be'
  const otHash = oneTimeHash(accountAddress)
  // const tsts = await r.get(otHash)
  // console.log('tsts', tsts)
  console.log('otHash', otHash)

  let transporter = nodemailer.createTransport({
    host: `${process.env.EMAIL_HOST}`,
    port: process.env.EMAIL_PORT,
    auth: {
      // NOTE: This might be good for checking a signed txn or direct wallet send
      user: accountAddress,
      pass: otHash,
    },
    secure: true,
    customAuth: {
      XBLOCKCHAIN: async ctx => {
        // TODO: check the hash given by the contract against blockchain
        // console.log('ctx.auth', ctx.auth)
        if (
          !ctx.auth.credentials.user ||
          !ctx.auth.credentials.pass
        ) return ctx.reject(new Error('Invalid username or password'))

        return true
      }
    },
    logger: true,
    debug: false, // include SMTP traffic in the logs
    tls: {
      rejectUnauthorized: false
    },
  })

  let message = {
    from: `<${accountAddress}@liame.app>`,
    to: `<${otHash}@liame.app>`,
    subject: 'OT HASH TEST',
    text: 'This test was successful',
  }

  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log('Error occurred')
      console.log(error.message)
      return process.exit(1)
    }
    console.log('info', info)
    console.log('Message sent successfully!')

    // only needed when using pooled connections
    transporter.close()
    process.exit()
  })
}, 500)
