import Web3Data from 'web3data-js'

export default {
  init({ apiKey, blockchainId, events }, callback) {
    const w3d = new Web3Data(apiKey, { blockchainId })
    w3d.connect()

    // Listen for all events, and trigger callback if new stuff
    if (events && events.length > 0) {
      events.forEach(e => {
        w3d.on(e, d => {
          if (callback) callback(e, d)
        })
      })
    }
  }
}
