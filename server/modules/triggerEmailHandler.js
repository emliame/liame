const nodemailer = require('nodemailer')
require('dotenv').config()

// Message object
const formatMessage = message => {
  if (!message.to) throw new Error('Missing message "to" email!')
  return {
    // TODO: Check this!
    from: `${message.address}@liame.app`, // 'Liame <demo@liame.app>'
    to: `${message.to}@liame.app`, // 'Liame <demo@liame.app>'
    subject: message.subject || `New Message for ${message.to}`,
    // TODO: Change these to use gun.sea encryption?
    text: message.text || 'Hello friend, This is an automated message. Thanks, Liame',
    html: message.html || `<p>Hello friend,<br />This is an automated message.<br /><br />Thanks,<br />Liame</p>`,
  }
}

function emailSend(message) {
  return new Promise((resolve, reject) => {
    // Create a SMTP transporter object
    const transporter = nodemailer.createTransport({
      host: `${process.env.EMAIL_HOST}`,
      port: `${process.env.EMAIL_PORT}`,
      auth: {
        type: 'custom',
        method: 'MY-CUSTOM-METHOD', // forces Nodemailer to use your custom handler
        user: 'username',
        pass: 'verysecret',
        options: {
          clientId: 'verysecret',
          applicationId: 'my-app'
        }
      },
      secure: false,
      customAuth: {
        'MY-CUSTOM-METHOD': async ctx => {
          // console.log('ctx.auth', ctx.auth)
          return true
        }
      },
      logger: false,
      debug: false, // include SMTP traffic in the logs
      tls: {
        rejectUnauthorized: false
      },
    }, {
      from: message.from.replace(/0x/g, ''), // 'Liame <demo@liame.app>',
      // headers: {
      //   'X-Blockchain-Id': 'fdsafdsafdsaf'
      // }
    })

    transporter.sendMail(message, (error, info) => {
      if (error) {
        console.log('Error occurred')
        console.log(error.message)
        // return process.exit(1)
        reject(error.message)
      }
      // console.log('Message sent successfully!')
      // console.log('info', info)
      resolve(info)
    })
  })
}

export default {
  async send({ address, message, otHash }) {
    const packet = formatMessage({ ...message, address, to: otHash })

    try {
      const res = await emailSend(packet)
    } catch (e) {
      console.log('send e', e)
    }
    // console.log('send res', res)
  }
}
