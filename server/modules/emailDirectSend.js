const crypto = require('crypto')
const nodemailer = require('nodemailer')
const redis = require('redis')
// const EmbarkJS = require('../../embarkArtifacts/embarkjs')
// const LiamPermissions = require('../../embarkArtifacts/contracts/LiamPermissions.js')
import UserPrefs from '../../embarkArtifacts/contracts/UserPrefs.js'
require('dotenv').config()

class RedisProvider {
  constructor() {
    this.client = null
    this.status = null

    return this
  }

  disconnect() {
    this.client.quit()
  }

  connect() {
    if (!this.client) this.client = redis.createClient()

    // errors? sure, lets see em
    this.client.on('error', err => {
      this.status = 'error'
      console.log(err)
    })

    this.client.on('ready', () => {
      this.status = 'ready'
      console.log('REDIS: ready')
    })

    return this.client
  }

  set(key, value) {
    if (!this.client) return Promise.resolve(null)
    return new Promise((resolve, reject) => {
      this.client.set(key, value, (err, reply) => {
        // callback
        console.log('SET redis:', reply)
        resolve(reply)
      })
    })
  }

  get(key) {
    if (!this.client) return Promise.resolve(null)
    return new Promise((resolve, reject) => {
      this.client.get(key, (err, reply) => {
        if (err) {
          reject(err)
          return
        }

        // reply is null when the key is missing
        console.log('GET redis:', reply)
        resolve(reply)
      })
    })
  }
}

const r = new RedisProvider()
r.connect()

const immediate = async (params) => {
  const otHash = params.to.split('@')[0].replace('0x', '')
  const hasAccess = await UserPrefs.methods.checkAccess(params.category, params.contractAddress).call()
  if (!hasAccess) return Promise.reject('Access Denied')
  r.set(otHash, params.prefsAddress)
  console.log('params.contractAddress', otHash, params.contractAddress)

  let transporter = nodemailer.createTransport({
    host: `${process.env.EMAIL_HOST}`,
    port: process.env.EMAIL_PORT,
    auth: {
      type: 'custom',
      method: 'XBLOCKCHAIN',
      user: params.contractAddress,
      pass: otHash
    },
    secure: false,
    customAuth: {
      XBLOCKCHAIN: async ctx => {
        // TODO: check the hash given by the contract against blockchain
        // console.log('ctx.auth', ctx.auth)
        if (
          !ctx.auth.credentials.user ||
          !ctx.auth.credentials.pass
        ) return ctx.reject(new Error('Invalid username or password'))

        return true
      }
    },
    logger: true,
    debug: false, // include SMTP traffic in the logs
    tls: {
      rejectUnauthorized: false
    },
  })

  const from = params.from
  const to = params.to
  let message = {
    from,
    to,
    subject: params.subject || `New Message from ${params.contractAddress}`,
    text: params.text || 'Please check your account for updates!',
    html: params.html || 'Please check your account for updates!',
  }

  return transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log('Error occurred')
      console.log(error.message)
      return process.exit(1)
    }
    console.log('info', info)
    // console.log('Message sent successfully!')

    // only needed when using pooled connections
    transporter.close()
  })
}

export default {
  immediate,
}
