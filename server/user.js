import dotenv from 'dotenv'
import redis from './modules/redis'
dotenv.config('../.env')

export default {
  setProxy: async user => {
    redis.connect()
    console.log('user', user)
    // TODO: Check user wallet and user deployed preferences contract for PROXY bool

    // Redis store user email if PROXY=true
    //    public address => email
    await redis.set(`${user.address}_proxy`, user.email)
  },
  getInbox: async user => {
    redis.connect()
    console.log('user', user)
    // Based on authed user, return inbox
    // NOTE: this assumes the messages are fully encrypted and user decrypts client side
    const data = await redis.get(user.address)
    console.log('getInbox data', data)
    return data
  }
}
