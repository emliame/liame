import dotenv from 'dotenv'
import emailHandlers from './modules/emailHandlers'
import emailDirectSend from './modules/emailDirectSend'
const { SMTPServer } = require('smtp-server')
dotenv.config('../.env')

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const server = new SMTPServer({
  // secure: true,
  // key: fs.readFileSync("private.key"),
  // cert: fs.readFileSync("server.crt")
  ...emailHandlers.defaultConfig,
  ...emailHandlers,
  tls: {
    rejectUnauthorized: false
  }
})

server.listen(process.env.EMAIL_PORT, process.env.EMAIL_HOST, r => {
  console.log(`LIAME SMTP STARTED: ${process.env.EMAIL_HOST}:${process.env.EMAIL_PORT}`)
})

server.on('error', e => {
  console.log('error', e)
})

export default {
  emailDirectSend,
}
