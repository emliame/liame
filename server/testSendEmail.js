const nodemailer = require('nodemailer')
require('dotenv').config()

// Generate SMTP service account from ethereal.email
nodemailer.createTestAccount((err, account) => {
  if (err) {
    console.error('Failed to create a testing account')
    console.error(err)
    return process.exit(1)
  }
  // console.log('account', account)
  console.log('Credentials obtained, sending message...')

  // NB! Store the account object values somewhere if you want
  // to re-use the same account for future mail deliveries

  // account.smtp = { host: process.env.EMAIL_HOST, port: process.env.EMAIL_PORT, secure: false }
  // account.user = 'demo@liame.app'
  // account.pass = 'derps'
  // account.method = 'PLAIN'
  // console.log('account', account)

  // // Create a SMTP transporter object
  // let transporter = nodemailer.createTransport({
  //   // host: account.smtp.host,
  //   // port: account.smtp.port,
  //   host: `${process.env.EMAIL_HOST}`, // smtp.
  //   port: process.env.EMAIL_PORT,
  //   // auth: {
  //   //     // method: 'LOGIN',
  //   //     user: 'demo@liame.app',
  //   //     pass: 'derps'
  //   // },
  //   // secure: account.smtp.secure,
  //   // secure: true,
  //   // auth: {
  //   //   user: account.user,
  //   //   pass: account.pass,
  //   //   // imap: { host: 'imap.ethereal.email', port: 993, secure: true },
  //   //   // pop3: { host: 'pop3.ethereal.email', port: 995, secure: true },
  //   // },
  //   logger: true,
  //   debug: false, // include SMTP traffic in the logs
  //   tls: {
  //     rejectUnauthorized: false
  //   },
  //   from: 'Liame <demo@liame.app>',
  //   headers: {
  //     'X-Blockchain-Id': 'fdsafdsafdsaf'
  //   }
  // }, {
  //   from: 'Liame <demo@liame.app>',
  //   headers: {
  //     'X-Blockchain-Id': 'fdsafdsafdsaf'
  //   }
  // })
  // Create a SMTP transporter object
  let transporter = nodemailer.createTransport({
    host: `${process.env.EMAIL_HOST}`, // smtp.
    port: process.env.EMAIL_PORT,
    auth: {
      type: 'custom',
      method: 'MY-CUSTOM-METHOD', // forces Nodemailer to use your custom handler
      user: 'username',
      pass: 'verysecret',
      options: {
        clientId: 'verysecret',
        applicationId: 'my-app'
      }
    },
    secure: false,
    customAuth: {
      'MY-CUSTOM-METHOD': async ctx => {
        console.log('ctx.auth', ctx.auth)
        return true
      }
    },
    logger: true,
    debug: false, // include SMTP traffic in the logs
    tls: {
      rejectUnauthorized: false
    },
  }, {
    from: 'Liame <demo@liame.app>',
    headers: {
      'X-Blockchain-Id': 'fdsafdsafdsaf'
    }
  })

  // Message object
  let message = {
    // Comma separated list of recipients
    to: 'Liame <demo@liame.app>',
    subject: 'Check this out! 🚀',
    text: 'Hello to myself!',
    html: `<p><b>Hello</b> to myself,<br /><br /><br />Thanks,<br />Liame</p>`,
  }

  transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log('Error occurred')
      console.log(error.message)
      return process.exit(1)
    }
    console.log('info', info)

    console.log('Message sent successfully!')
    console.log(nodemailer.getTestMessageUrl(info))

    // only needed when using pooled connections
    transporter.close()
  })
})
