pragma solidity ^0.6.0;
import "./ecdsa.sol";

/*
 * 0 - not set
 * 1 - allow email from this account/dApp
 * 0 - do not allow from email from thie account/dApp
 */

contract UserPrefs is ECDSA {
  address owner;
  mapping(bytes32 => uint8) liameAccess;
  bool public useProxy;

  constructor() public {
    owner = msg.sender;
  }

  function checkAccess(string memory category, address fromdApp) public view returns (uint8 hasAccess) {
    return liameAccess[_getHash(category, fromdApp)];
  }

  function _getHash(string memory category, address fromdApp) private pure returns (bytes32) {
    return keccak256(abi.encodePacked(fromdApp, bytes(category)));
  }

  function setAccess(uint8 isAllowed, string memory category, address fordApp) public {
    // only the owner can update their own preferences
    require(msg.sender == owner, "You are not allowed to set this");
    bytes32 accessHash = _getHash(category, fordApp);
    liameAccess[accessHash] = isAllowed;
  }

  function getAccess(string memory category, address fromdApp) public view returns (bytes32) {
    bytes32 accessHash = _getHash(category, fromdApp);
    require(liameAccess[accessHash] == 1, "Not allowed");
    return accessHash;
  }

  function setProxy(bool _proxy) public {
    require(msg.sender == owner, "Only owner can update");
    useProxy = _proxy;
  }
}
