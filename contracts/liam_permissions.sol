pragma solidity ^0.6.0;

interface UserPrefsInterface {
  function checkAccess(string calldata _category, address _address) external view returns(uint8);
}

contract LiamPermissions {
  constructor() public {}

  // Read the category permission for the specified target address
  function readPermissions(
    address _targetContactAddress,
    address _targetWalletAddress,
    string memory category
  ) public returns (uint8 isPermissioned) {

    // Using interface syntax instead of "checkAccess(string, address)"
    address tca = _targetContactAddress;
    (bool success, bytes memory result) = tca.delegatecall(
      abi.encodeWithSignature("checkAccess(string,address)",
      category,
      _targetWalletAddress)
    );

    return abi.decode(result, (uint8));
  }
}
