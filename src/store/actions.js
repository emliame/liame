import Torus from '@toruslabs/torus-embed'
import Web3 from 'web3'

export default {
  async init({ commit, state }) {
    try {
      const torus = new Torus({ buttonPosition: 'top-right' })

      window.torus = torus
      await torus.init({
        buildEnv: this.buildEnv,
        enabledVerifiers: { reddit: false },
        enableLogging: true,
        network: { host: 'rinkeby', chainId: 4 },
        showTorusButton: false
      })
      // await torus.login()
      // const web3 = new Web3(torus.provider)
      // torus.provider.on('chainChanged', (resp) => console.log(resp, 'chainchanged'))
      // window.web3 = web3
      // commit('UPDATE', { key: 'hasProvider', value: true })
      // commit('UPDATE', { key: 'connected', value: true })
    } catch (error) {
      console.error(error)
    }
  },

  async login({ commit, state }) {
    try {
      const torus = new Torus({ buttonPosition: 'top-right' })

      window.torus = torus
      await torus.init({
        buildEnv: this.buildEnv,
        enabledVerifiers: { reddit: false },
        enableLogging: true,
        network: { host: 'rinkeby', chainId: 4 },
        showTorusButton: true
      })
      await torus.login()
      const web3 = new Web3(torus.provider)
      torus.provider.on('chainChanged', (resp) => console.log(resp, 'chainchanged'))
      window.web3 = web3
      commit('UPDATE', { key: 'hasProvider', value: true })
      commit('UPDATE', { key: 'connected', value: true })

      web3.eth.getAccounts().then(accounts => {
        const address = accounts[0]
        console.log('address', address)
        // Is this needed?
        web3.eth.getBalance(address).then(balance => {
          console.log('balance', balance)
        })

        // Account is logged in, set our state for later use
        commit('UPDATE', { key: 'account', value: address })
        // loop through the various categories and see if they're already set
        console.log('tired categories', state.categories);

        // popup the modal
        commit('UPDATE', { key: 'showModal', value: true })
      })
    } catch (error) {
      console.error(error)
    }
  },

  logout({ commit }) {
    window.torus.logout().then(() => {
      commit('UPDATE', { key: 'account', value: null })
    })
  },

  async getUserInfo() {
    const info = await window.torus.getUserInfo().catch(console.log)
    console.log('getUserInfo', info)
    return info
  },

  // whatever it works, v is not working properly
  async updateState({commit}, args) {
    commit('UPDATE', args)
  },

  getPublicAddress({ state }) {
    console.log(state.selectedVerifier, state.verifierId)
    return window.torus.getPublicAddress({
      verifier: state.selectedVerifier,
      verifierId: state.verifierId
    })
  },

  // NOTE: Torus backup methods
  // sendEth() {
  //   window.web3.eth.sendTransaction({ from: this.publicAddress, to: this.publicAddress, value: window.web3.utils.toWei('0.01') })
  // },
  // signMessage() {
  //   const self = this
  //   // hex message
  //   const message = '0x47173285a8d7341e5e972fc677286384f802f8ef42a5ec5f03bbfa254cb01fad'
  //   window.torus.web3.currentProvider.send(
  //     {
  //       method: 'eth_sign',
  //       params: [this.publicAddress, message],
  //       from: this.publicAddress
  //     },
  //     function(err, result) {
  //       if (err) {
  //         return console.error(err)
  //       }
  //       self.console('sign message => true \n', result)
  //     }
  //   )
  // },
}
